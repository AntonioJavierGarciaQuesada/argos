'use strict'

import jwt from 'jsonwebtoken'
import moment from 'moment'
import clave from '../services/clave'

exports.ensureAuth = function(req,res,next){
    
    if(!req.headers.authorization){
        return res.status(403).send({
            mensaje:'La petición no tiene la cabecera de autenticación'
        })
    }
    let token = req.headers.authorization.replace(/['"]+/g,'')
    if(!token)   return res.status(401).send({
        mensaje:'La petición no tiene la cabecera de autenticación'
    }) 
    jwt.verify(token,clave.secret,(err,payload)=>{
        if(err) return res.status(401).send({
            mensaje:'Error: No se le permite realizar peticiones'})
            req.user = payload;
            next()
        })
}
