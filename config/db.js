import mongoose from 'mongoose'


// Le indicamos a Mongoose que haremos la conexión con Promesas
mongoose.Promise = global.Promise;
// Usamos el método connect para conectarnos a nuestra base de datos

mongoose.connect('mongodb://axtraer:leonidas35@ds249079.mlab.com:49079/reformas')
    .then(() => { 
        // Cuando se realiza la conexión, lanzamos este mensaje por consola
        console.log('¡¡La conexión a la base de datos de reformas se ha realizado correctamente!!');           
    
    })
    .catch( (err) => console.log(err))
    // Si no se conecta correctamente escupimos el error
    