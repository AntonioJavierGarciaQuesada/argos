//import modulos instalados
import express from 'express'
import cors from 'cors' //Para las cabeceras

//import rutas
import ruta_usuario from '../routes/ruta_usuario'
import ruta_contacto from '../routes/ruta_contacto'
import ruta_mensaje from '../routes/ruta_mensajes'
import ruta_proyecto from '../routes/ruta_proyectos'
//instanciamos nuestro servidor con express y configuramos el uso del body-parser y de las cabeceras
const app = express()

//middlewares
app.use(express.json())
app.use(cors())
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});
//routes principales
app.use('/',ruta_usuario)
app.use('/contacto',ruta_contacto)
app.use('/mensaje',ruta_mensaje)
app.use('/proyecto',ruta_proyecto)





export default app
