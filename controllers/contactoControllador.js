'use strict'

import nodemailer from 'nodemailer'
import mg from 'nodemailer-mailgun-transport'
import mailgun from '../config/mailgun.json'
import sgMail from '@sendgrid/mail'
import api_key from '../config/sendgrid'


function prueba(req, res){
    res.status(200).send({mensaje:'Controlador de contacto funcional'})
}
function sendEmail(req,res){
    let params = req.body
    if(!params.nombre || !params.apellidos || 
        !params.emailC || !params.texto)
        return res.status(400).send({mensaje:"Error faltan campos Obligatorios"})
    let transporter = nodemailer.createTransport(mg(mailgun))
    let email = {
        from: '"NodeMail" <test@sandbox7d2243d653a94ae996e2ecd4fb152127.mailgun.org>',
        to: 'axtraer@gmail.com',
        subject: 'Mensaje desde el formulario de contacto',
        text: `El remitente ${params.nombre} ${params.apellidos} con email de contacto ${params.emailC},
        manda el siguiente mensaje: ${params.texto}`,
        err: false  
    } 
    transporter.sendMail(email, function (error, info) {
        if (error) {       
            return res.status(500).send({ mensaje: 'Error: '+error })
        } else {
            return res.status(200).send({mensaje: 'mensaje enviado correctamenente'});
        }  
    })     
}

function sendEmailSendGrid(req,res){
    sgMail.setApiKey("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")
    let params = req.body
    if(!params.nombre || !params.apellidos || 
        !params.emailC || !params.mensaje)
        return res.status(400).send({mensaje:"Error faltan campos Obligatorios"})
   
    let email = {
        from: 'contacto@tureforma.es',
        to: 'axtraer@gmail.com',
        subject: 'Mensaje desde el formulario de contacto',
        text: `El remitente ${params.nombre} ${params.apellidos} con email de contacto ${params.emailC},
        manda el siguiente mensaje: ${params.mensaje}`        
    } 
    sgMail.send(email)        
}

module.exports = {
    prueba,
    sendEmail,
    sendEmailSendGrid
}