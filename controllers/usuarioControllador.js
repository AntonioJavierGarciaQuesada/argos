'use strict'
import UsuarioModel from '../models/usuarioModelo'
import bcrypt from 'bcrypt'
import Debug from 'debug'
import Mjwt from '../services/jwt' 
import fs from 'fs'
import path from 'path'
import mongoosePaginate from 'mongoose-pagination'
import nodemailer from 'nodemailer'
import mg from 'nodemailer-mailgun-transport'
import mailgun from '../config/mailgun.json'

import { EBUSY } from 'constants'

const debug = new Debug('debug-argos:auth')

const BCRYPT_SALT_ROUNDS = 12;


/**
 * @function login funcion que logea a un usuario siempre que este en la base de datos enviandole un webtoken
 * @param {*} req parametro que contiene el email y el password del usuario a logear 
 * @param {*} res devuelve en caso de logueo correcto un objeto con un token de autenticación y datos del usuario
 * @return res devuelve una objeto con datos del usuario y webtoken y en caso contrario un mensaje de error 
 */
function login(req,res) {

    let email = req.body.email.toLowerCase()
    let password = req.body.password
    
    UsuarioModel.findOne({'email':email}).exec((err, usuario) => {
        if(err) return res.status(500).send({message: 'Error en el servidor: '+err})
       
            // Devolvemos el resultado de la query en json
            if(usuario){
                
                bcrypt.compare(password,usuario.password)
                    .then((passValidate)=>{
                      
                        if ( passValidate ){
                            const token = Mjwt.createToken(usuario)// jwt.sign({ usuario }, secret,{expiresIn:"1d"})
                            usuario.password = undefined
                            return res.status(200).send({
                                message: 'Login succeded',
                                token,
                                usuario
                            })                            
                        } else {
                            return res.status(200).send({
                                message: 'No hay usuario con la combinación email y password registrados'
                            })    
                        }
                    })                   
            }else{
                return res.status(200).send({
                    message: 'No hay usuario con la combinación email y password registrados'
                })
            }
         
    })    
}


/**
 * @function saveUser funcion que registra un usuario y almacena en la base de datos
 * @param {*} req parametro que contiene los datos de un usuario
 * @param {*} res devuelve en caso de logueo correcto un objeto con un token de autenticación y datos del usuario
 * @return res devuelve una objeto con datos del usuario y webtoken y en caso contrario un mensaje de error  
 */
function saveUser(req,res) {

    let usuario = new UsuarioModel()  
    
    const params = req.body  
   
    if( params.email && params.password 
        && params.tipo && params.nombreUsuario
       ){
  
        usuario.email = params.email.toLowerCase()
        usuario.tipo = params.tipo 
        usuario.nombreUsuario = params.nombreUsuario.toLowerCase()
        
        
        UsuarioModel.find({
            $or:[
                {email: usuario.email},
                {nombreUsuario: usuario.nombreUsuario}
            ]
        }).exec((err, usuarios)=>{
            if(err) return res.status(500).send({
                message: 'Error en la peticion de registro usuario'
            })
            
            if(usuarios && usuarios.length >=1){
                return res.status(400).send({
                    message: 'El usuario que intenta registrar ya existe'
                })   
            }else{
                //encriptacion de la clave y almacenamiento en la base de datos y creaacion de webtoken
                bcrypt.hash(params.password,BCRYPT_SALT_ROUNDS)
                    .then((hashedPassword,err) => {
                        if(err) return res.status(500).send({
                                    message: 'Error en la peticion de registro usuario'
                                })
                        
                        usuario.password = hashedPassword
                        usuario.save((err, usuarioStored) => {
                            if(err) return res.status(500).send({message: 'Error en el servidor: '+err})
                            // En el caso de que el documento se guarde tambien devolvemos el objeto guardado
                            if(usuarioStored){
                            
                                const token = Mjwt.createToken(usuarioStored)// jwt.sign({ usuario }, secret,{expiresIn:"1d"})
                                usuarioStored.password = undefined
                                return res.status(200).send({
                                    message: 'Login succeded',
                                    token,
                                    usuarioStored                                    
                                })
                                
                            }else{
                                    return res.status(404).send({
                                        message: 'No se ha guardado el usuario'
                                    })
                            }                     
                        })
                    })
            }                        
        })
        
        
    }else{
        return res.status(400).send({
            message: 'Envia todos los campos obligatorios'
        })    
    
    }    
    
}

function getUserById(req,res){

    let userId = req.params.id;

    UsuarioModel.findById(userId, (err,usuario)=>{
        if(err) return res.status(500).send({mensaje: 'Error en la petición'})

        if(!usuario) return res.status(404).send({mensaje: 'El usuario no existe'})
        usuario.password = undefined
        return res.status(200).send({usuario})
    })
}

function getUsers(req,res){
    
    if(req.user.payload.tipo !== "Administrador") return res.status(401).send({mensaje:'No tiene permiso para realizar esta peticion'})
    let identity_user_id = req.user.payload.sub
    let page = 1
    let itemsPerPage = 5
    if(req.params.page){
        page = req.params.page
    }

    UsuarioModel.find().sort('_id').paginate(page,itemsPerPage,(err,users,total)=>{
        if( err ) return res.status(500).send({mensaje: 'Error en la petición'})
        if( !users) return res.status(200).send({mensaje: 'No hay usuarios disponibles'})
        return res.status(200).send({
            users,
            total,
            pages: Math.ceil(total/itemsPerPage)
        })
    })   
    
}

function getAutonomos(req,res){
    
    
    UsuarioModel.find({tipo:"Autonomo"},(err,autonomos) => {
        if( err ) return res.status(500).send({mensaje: 'Error en la petición'})
        if( !autonomos ) return res.status(200).send({status:"error",mensaje: 'No hay usuarios disponibles'})       
        return res.status(200).send({status:"success",autonomos})
    })  
    
}

function getClientes(req,res){
     
    UsuarioModel.find({tipo:"Cliente"},(err,clientes) => {
        if( err ) return res.status(500).send({mensaje: 'Error en la petición'})
        if( !clientes ) return res.status(200).send({status:"error",mensaje: 'No hay usuarios disponibles'})       
        return res.status(200).send({status:"success",clientes})
    })  
}

function updateUser(req,res){
    let userId = req.params.id
    let update = req.body
    console.log(update)

    //borrar la propiedad password
    delete update.password

    if(userId != req.user.payload.sub && req.user.payload.tipo != 'Administrador') return res.status(401).send({mesanje: 'No tienes permiso para actualizar los datos del usuario'})
    //Nos devuelve el objeto actualizado new:true
    UsuarioModel.findByIdAndUpdate(userId, update,{new:true},(err, userUpdate)=>{
        if(err) return res.status(500).send({mensaje: 'Error en la petición'})
        if(!userUpdate) return res.status(404).send({mensaje: 'No se ha podido actualizar el usuario'})
        userUpdate.password = undefined
        return res.status(200).send({user: userUpdate})
    })
}

function uploadImage(req,res){
    let userId = req.params.id    
    
    if(req.files){
        let file_path = req.files.image.path
        let file_split = file_path.split('/')  
        let file_name = file_split[2]
        let file_ext = file_name.split('\.')[1]
        
        if(userId != req.user.payload.sub && req.user.payload.tipo != 'Administrador'){
            return removeFilesOfUploads(res,file_path,'No tiene permisos para subir ficheros')
        }else{
            if(['png','jpg','jpeg','bmp','gif'].indexOf(file_ext) != -1){
                UsuarioModel.findByIdAndUpdate(userId,{avatar: file_name},{new:true},(err,userUpdate)=>{
                    if(err) return res.status(500).send({mensaje: 'Error en la petición'})
                    if(!userUpdate) return res.status(404).send({mensaje: 'No se ha podido actualizar el usuario'})
                    userUpdate.password = undefined
                    console.log(userUpdate)
                    return res.status(200).send({user: userUpdate})
                })
            }else{
               return removeFilesOfUploads(res,file_path,'Error: Archivo no valido')
            }
        }
     
    }else{
        return res.status(400).send({mensaje: 'No se ha subido archivos'})
    }
}

function removeFilesOfUploads(res,file_path,mensaje){
    fs.unlink(file_path,(err)=>{
        return res.status(200).send({status:"error"+err,mensaje:mensaje})
    })
}

function getImageFile(req,res){
    let image_file = req.params.imageFile
    let path_file = './uploads/users/'+image_file;

    fs.exists(path_file, (exits) =>{
        if(exits) return res.sendFile(path.resolve(path_file))
        return res.status(401).send({mensaje: 'No existe la imagen'})
    })
}


function actualizarPassword(res,email,password){
    
    bcrypt.hash(password,BCRYPT_SALT_ROUNDS)
    .then((hashedPassword,err) => {
        if(err) return res.status(500).send({
                    message: 'Error en la peticion de recuperar contraseña'
                })
        UsuarioModel.findOneAndUpdate({email:email},{password:hashedPassword},(err)=>{
            if(err) return res.status(500).send({message: 'Error en el servidor: '+err})
        })   
    })           
}

function recuperarPassword(req,res){
    if(!req.body.email)  return res.status(400).send({ mensaje: 'Error el email es un campo obligatorio' })
    
    let emailUsuario = req.body.email
     
   return UsuarioModel.findOne({email:emailUsuario})
    .exec((err, usuario)=>{  
        if(err) return res.status(404).send({mensaje:"Error"+err})
        if(!usuario) return res.status(200).send({ mensaje: 'Error el email no es valido' })
        
        let transporter = nodemailer.createTransport(mg(mailgun))
        let password = generarPassword()
        actualizarPassword(res,emailUsuario,password)
        let email = {
            from: '"NodeMail" <test@sandbox7d2243d653a94ae996e2ecd4fb152127.mailgun.org>',
            to: emailUsuario,
            subject: "Recueperación contraseña de Tureforma.es",
            text: "Buenos dias a su solicitud de recuperación le mandamos la siguiente contraseña Email: "+emailUsuario+" Contraseña: "+password ,
            err: false
        }
        
        transporter.sendMail(email,(err,info)=>{
            if (err){
                
                return res.status(500).send({ mensaje: 'error al enviar correo' })
            } 
            return res.status(200).send({mensaje: 'Email enviado correctamente con su nueva contraseña'})
        })
    })   
    
}
///MODIFICAR

function cambiarPassword(req,res){
    let pass = req.body
   
    if(!pass || !pass.pass_old || !pass.pass_new ) return res.status(400).send({mensaje:'Faltan campos obligatorios'})
    
    bcrypt.compare( pass.pass_old,req.user.payload.password)
    .then((passValidate) =>{
        if( !passValidate ) return res.status(200).send({status:'error',mensaje:'Contraseña invalidad'})
        
        bcrypt.hash(pass.pass_new,BCRYPT_SALT_ROUNDS)
        .then((hashedPassword,err) => {
            if(err) return res.status(500).send({
                        message: 'Error en la peticion de recuperar contraseña'
                    })
            UsuarioModel.findOneAndUpdate({email:req.user.payload.email},{password:hashedPassword},(err)=>{
                if(err) return res.status(500).send({message: 'Error en el servidor: '+err})
            })   
        })           
        return res.status(200).send({status:'success',mensaje:'Contraseña actualizada correctamente'})
    })
    
}

function generarPassword() {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@-+.,";
  
    for (var i = 0; i < 10; i++){
      text += possible.charAt(Math.floor(Math.random() * possible.length))
    }    
    return text;
}





module.exports = {
    login,
    saveUser,
    getUserById,
    getUsers,
    updateUser,
    uploadImage,
    getImageFile,
    recuperarPassword,
    cambiarPassword,
    getAutonomos,
    getClientes

} 