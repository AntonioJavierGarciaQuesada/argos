'use strict'

import moment from 'moment'
import mgPaginate from 'mongoose-pagination'
import UsuarioModel from '../models/usuarioModelo'
import MensajeModel from '../models/mensajeModelo'
import ProyectoModel from '../models/proyectoModelo'

function test(req,res){
    return res.status(200).send({mensaje: "Controlador de mensajes funcional"})

}

function saveMensaje(req,res){
    let params = req.body

    if(!params.texto || !params.receptor ) return res.status(400).send({mensaje: "Error: Rellena los campos obligatorios"})
    let mensaje = new MensajeModel()
    mensaje.emisor = req.user.payload.sub
    mensaje.receptor = params.receptor    
    mensaje.texto = params.texto
    mensaje.fecha_creacion = moment().unix()
   
    mensaje.save((err,mensajeStored)=>{
        if(err) return res.status(500).send({mensaje: 'Error en la peticion de envio mensaje'+ err})
        if(!mensajeStored) return res.status(500).send({mensaje: "Error al enviar el mensaje"}) 
        
        return res.status(200).send({mensaje: mensajeStored})
    })    
}

function getRecivedMensajes(req,res){
    let userId = req.user.payload.sub
    let page = 1;
    if(req.params.page) page = req.params.page
    let itemsPerPage = 1000
    MensajeModel.find({receptor:userId}).populate('receptor emisor','perfil nombreUsuario email').paginate(page,itemsPerPage,(err,mensajes,total)=>{
        if(err) return res.status(500).send({mensaje: 'Error en la peticion de envio mensaje'+ err})
        if(!mensajes) return res.status(404).send({mensaje: 'No hay mensaje que mostrar'})
        return res.status(200).send({
            total,
            page: Math.ceil(total/itemsPerPage),
            mensajes
        })
    })

}

function getEmmitMensajes(req,res){
    let userId = req.user.payload.sub
    let page = 1;
    if(req.params.page) page = req.params.page
    let itemsPerPage = 1000
    MensajeModel.find({emisor:userId}).populate('receptor emisor','perfil nombreUsuario email').paginate(page,itemsPerPage,(err,mensajes,total)=>{
        if(err) return res.status(500).send({mensaje: 'Error en la peticion de envio mensaje'+ err})
        if(!mensajes) return res.status(404).send({mensaje: 'No hay mensaje que mostrar'})
        return res.status(200).send({
            total,
            page: Math.ceil(total/itemsPerPage),
            mensajes
        })
    })

}
function getUnviewdMensajes(req,res){
    let userId = req.user.payload.sub
    MensajeModel.count({receptor:userId,viwed:'false'}).exec((err,count)=>{
        if(err) return res.status(500).send({mensaje: 'Error en la peticion de envio mensaje'+ err})
        return res.status(200).send({unviewed:count})
    })
}

function setViewedMensaje(req,res){
    let userId = req.user.payload.sub

    MensajeModel.update({receptor:userId, viwed:false},{viwed:true},{'multi':true},(err,mensajeUpdate)=>{
        if(err) return res.status(500).send({mensaje: 'Error en la peticion de envio mensaje'+ err})
        if(!mensajeUpdate) return res.status(404).send({mensaje: "No se ha podido actualizar"})
        return res.status(200).send({mensaje:mensajeUpdate})
    })
}

module.exports = {
    test,
    saveMensaje,
    getRecivedMensajes,
    getEmmitMensajes,
    getUnviewdMensajes,
    setViewedMensaje
}