' use strict'

import path from 'path'
import fs from 'fs'
import moment from 'moment'
import ProyectoModel from '../models/proyectoModelo'
import UsuarioModel from '../models/proyectoModelo'


function test(req,res){
   return res.status(200).send({
        mensaje: 'Controlador proyectos funcional'
    })
}

function saveProyecto(req,res){
    let params = req.body
    
    if( !params.categoria || !params.descripcion_breve || !params.descripcion_completa ||
        !params.precio || !params.size || !params.direccion || !params.localidad || 
        !params.provincia || !params.comunidad || !params.incluir_material) 
        return res.status(200).send({status:"error",mensaje:"Faltan campos obliagatorios"})
        
    let proyecto = new ProyectoModel()
    proyecto.cliente = req.user.payload.sub
    proyecto.categoria = params.categoria
    proyecto.descripcion_breve = params.descripcion_breve 
    proyecto.descripcion_completa = params.descripcion_completa 
    proyecto.precio = params.precio 
    proyecto.size = params.size 
    proyecto.direccion = params.direccion 
    proyecto.localidad = params.localidad 
    proyecto.provincia = params.provincia 
    proyecto.comunidad = params.comunidad
    proyecto.incluir_material = params.incluir_material
    proyecto.creado_en = moment().unix()
    proyecto.path_photos = 'null';

    proyecto.save((err,proyectoStored)=>{
        if(err) return res.status(500).send({mensaje: 'Error  interno en el servidor guardar el proyecto'+err})
        if(!proyectoStored) return res.status(404).send({mensaje:'Error:'+err })

        return res.status(200).send({status:"success",proyecto: proyectoStored})
    })
}

function getMisProyectos(req,res){
    let clienteId =  req.user.payload.sub

    ProyectoModel.find({cliente:clienteId}).exec((err,misProyectos)=>{
        if(err) return res.status(500).send({mensaje: 'Error  interno el devolver los proyecto'+err})
        if(!misProyectos) return res.status(404).send({mensaje:'Error:'+err })
        return res.status(200).send({status:"success",proyectos:misProyectos})
    }) 
}
function getProyectos (req,res){
    ProyectoModel.find().exec((err,proyectos)=>{
        if(err) return res.status(500).send({mensaje: 'Error  interno el devolver los proyecto'+err})
        if(!proyectos) return res.status(404).send({mensaje:'Error:'+err })
        return res.status(200).send({status:"success",proyectos:proyectos})
    })
}

module.exports = {
    test,
    saveProyecto,
    getProyectos,
    getMisProyectos
}
    

