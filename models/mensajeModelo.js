'use strict'

import mongoose from 'mongoose'
import autoIncrement from 'mongoose-auto-increment'

var Esquema = mongoose.Schema

var MensajeEsquema = new Esquema(
    {
        emisor:{
            type:Number,
            required: 'El emisor es un campo obligatorio',
            ref: 'usuarioModel'
        },
        receptor: {
            type: Number,
            required: 'El receptor es un campo obligatorio',
            ref: 'usuarioModel'
        },        
        texto:{
            type: String,
            required: 'El texto es un campo obligatorio'
        },        
        viwed:{
            type: Boolean,
            default: false
        }    
        
    },
    {
        timestamps:true
    }   
)
autoIncrement.initialize(mongoose.connection)
MensajeEsquema.plugin(autoIncrement.plugin, {
    model:'MensajeModel',
    field: '_id',
    startAt: '1',
    incrementBy: 1
})

module.exports = mongoose.model('MensajeModel',MensajeEsquema,'mensajes')