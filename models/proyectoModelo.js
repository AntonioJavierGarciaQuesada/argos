import mongoose from 'mongoose'
import autoIncrement from 'mongoose-auto-increment'
import moment from 'moment'

const Esquema = mongoose.Schema

const ProyectoEsquema = new Esquema(
    {
        cliente:{
            type:Number,
            required: 'El id de usuario es un campo obligatorio',
            ref: 'UsuarioModelo'
        },   
        categoria:{
            type: String,
            required: 'La categoria del proyecto es un campo obligatorio'
        },        
        descripcion_breve: {
            type:String,
            required: 'La descripción breve  es un campo obligatorio'            
        },
        descripcion_completa: {
            type:String,
            required: 'La descripción completa es un campo obligatorio'
        },
        precio:{
            type: Number,
            required: 'El precio es un campo obligatorio'
        },
        duracion: {
            type:String
        },
        size:{
            type:String,
            required: 'El tamaño del proyecto es un campo obligatorio',
            enum: ['Pequeño','Mediano','Grande']
        },        
        direccion:{
            type:String,
            required: 'direccion es un campo obligatorio',
        },
        localidad:{
            type:String,
            required: 'localidad es un campo obligatorio',
        },
        provincia:{
            type:String,
            required: 'provincia es un campo obligatorio'
        },
        comunidad:{
            type:String,
            required: 'comunidad es un campo obligatorio',
        },
        incluir_material:{
            type:Boolean,
            required: 'incluir material es un campo obligatorio'
        },
        path_photos:{
            type: String
        }
    },
    {
        timestamps:true
    }     
)
autoIncrement.initialize(mongoose.connection)
ProyectoEsquema.plugin(autoIncrement.plugin, {
    model:'ProyectoModel',
    field: '_id',
    startAt:'1',
    incrementBy: 1
})


module.exports = mongoose.model('ProyectoModel',ProyectoEsquema,'proyectos')