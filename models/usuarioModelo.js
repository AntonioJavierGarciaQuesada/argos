
import mongoose from 'mongoose'
import autoIncrement from 'mongoose-auto-increment'
import moment from 'moment'

const Esquema = mongoose.Schema

const UsuarioEsquema = new Esquema(
    {
        nombreUsuario: {
            type: String,
            required: 'El nombre de usuario es un campo obligatorio'
        },
        email:{
            type: String,
            required: 'El email de usuario es un campo obligatorio'
        },
        password: {
            type:String,
            required: 'El password de usuario es un campo obligatorio'
        },
        tipo: {
            type:String,
            required: 'El tipo de usuario es un campo obligatorio',
            enum: ['Administrador','Autonomo','Cliente']
        },
        isAvailable: {
            type: Boolean,
            default: false 
        },
        creat_ad: {
          type: Date
         
        },
        avatar: {
            type:String
        },
        perfil:{
            nombre:{
                type:String
            },
            apellidos:{
                type:String
            },
            dni:{
                type:String  
            },
            cif:{
                type:String 
            },
            telefono: {
                type:Number
            },
            movil: {
                type:Number
            },      
            fecha_nacimiento:{
                type:Date
            },        
            direccion: {
                type:String
            },
            localidad: {
                type:String
            },
            provincia: {
                type:String
            },
            comunidad: {
                type:String
            }              
        },
        experiencia: {
            tipo_habilidad:String,
            year:Number,
            descripcion:String,
            tipo_trabajo:String,
            salario_hora:Number
        }     
    },
    {
        timestamps:true
    }    
)
autoIncrement.initialize(mongoose.connection)
UsuarioEsquema.plugin(autoIncrement.plugin, {
    model:'usuarioModel',
    field: '_id',
    startAt: '1',
    incrementBy: 1
})

//usuarios -> nombre de la tabla


module.exports = mongoose.model('usuarioModel',UsuarioEsquema,'usuarios')