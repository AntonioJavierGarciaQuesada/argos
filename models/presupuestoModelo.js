'use strict'

import mongoose from 'mongoose'
var Esquema = mongoose.Schema

var PresupuestoEsquema = new Esquema(
    {
        autonomo: {
            type: Esquema.ObjectId,
            required: 'El campo autonomo es obligatorio',
            ref: 'UsuarioModel'
        },
        proyecto: {
            type: Esquema.ObjectId,
            required: 'El campo proyecto es obligatorio',
            ref: 'ProyectoModel'  
        },
        precio: {
            type: Number,
            required: 'El precio es un campo obligatorio'
        },
        tipo_pago:{
            type:String,
            required: 'El tipo de pago es un campo obligatorio',
            enum: ['total','horas']
        },
    },
    {
        timestamps:true
    }   
)
autoIncrement.initialize(mongoose.connection)
ProyectoEsquema.plugin(autoIncrement.plugin, {
    model:'PresupuestoModel',
    field: '_id',
    startAt:'1',
    incrementBy: 1
})

module.exports = mongoose.model('PresupuestoModel',PresupuestoEsquema,'presupuestos')