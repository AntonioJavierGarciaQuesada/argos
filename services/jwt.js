'use strict'

import jwt from 'jsonwebtoken'
import moment from 'moment'
import clave from './clave'

function createToken(usuario){
    let payload = {
        sub: usuario._id,
        password: usuario.password,
        email: usuario.email,
        tipo: usuario.tipo,
        nombreUsuario: usuario.nombreUsuario,
        iat: moment().unix()
    }
    
   return jwt.sign({payload}, clave.secret,{expiresIn:"30d"})
}

module.exports = {
    createToken 
} 