//importar modulos instalados
import http from 'http'
import Debug from 'debug'


//importamos la configuracion
import app from './config/app'
import db from './config/db'

//Definicions constantes PORT y debug
const PORT = process.env.PORT || 3700
const debug = new Debug('debug-argos:root')

//hacemos que el servidor se inicie y escuche en el puerto 3000
app.listen(PORT,()=>{
    debug(`server running at port ${PORT}`)
})
