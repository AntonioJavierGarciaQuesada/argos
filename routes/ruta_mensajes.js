'use strict'

import express from 'express'
import mensajeController from '../controllers/mensajeControllador'
import md_auth from '../middlewares/authentication'

let api = express.Router()

api.get('/',md_auth.ensureAuth, mensajeController.test)
api.post('/enviar',md_auth.ensureAuth, mensajeController.saveMensaje)
api.get('/mis-mensajes-recibidos/:page?',md_auth.ensureAuth,mensajeController.getRecivedMensajes)
api.get('/mis-mensajes-enviados/:page?',md_auth.ensureAuth,mensajeController.getEmmitMensajes)
api.get('/mensajes-noVisualizados',md_auth.ensureAuth,mensajeController.getUnviewdMensajes)
api.post('/actualizar-mensaje-leidos',md_auth.ensureAuth,mensajeController.setViewedMensaje)
module.exports = api