'use strict'

import express from 'express'

import concatoController from '../controllers/contactoControllador'


var api = express.Router()


//RUTAS
api.get('/', concatoController.prueba)
api.post('/',concatoController.sendEmail)

module.exports = api