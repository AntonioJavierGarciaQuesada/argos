'use strict'

import express from 'express'
import md_auth from '../middlewares/authentication'
import usuarioControllador from '../controllers/usuarioControllador'
import multipart from 'connect-multiparty'

var api = express.Router()
var md_upload = multipart({uploadDir: './uploads/users'})

//RUTAS de usuario

//Ruta login
api.post('/login', usuarioControllador.login)

//Ruta registro
api.post('/signin',usuarioControllador.saveUser)

//Ruta obtener usuario por su id
api.get('/user/:id',md_auth.ensureAuth,usuarioControllador.getUserById)

//Ruta obtener listado usuario paginados
api.get('/users/:page',md_auth.ensureAuth,usuarioControllador.getUsers)

//Ruta obtener listado de autonomos
api.get('/autonomos',usuarioControllador.getAutonomos)

//Ruta obtener listado de clientes
api.get('/clientes',md_auth.ensureAuth,usuarioControllador.getClientes)

//Ruta Actualizar usuario por id
api.put('/update-user/:id',md_auth.ensureAuth,usuarioControllador.updateUser)

//Ruta subir imagen avatar de un usuario
api.post('/upload-image-user/:id',[md_auth.ensureAuth,md_upload], usuarioControllador.uploadImage)

//Ruta para obtener ruta de imagen de un avatar de un usuario
api.get('/get-image-user/:imageFile',usuarioControllador.getImageFile)

//Ruta para recuperar password se envia por email
api.post('/recuperar-password',usuarioControllador.recuperarPassword)

//Ruta para cambiar password
api.put('/cambiar-password',md_auth.ensureAuth,usuarioControllador.cambiarPassword)
//Ruta de prueba
api.get('/home',md_auth.ensureAuth,function(req,res){
    res.status(200).send({
        mensaje:'Bienvenido a Argos'
    })
})


export default api