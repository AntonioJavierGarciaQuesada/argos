'use strict'

import express from 'express'
import proyectoController from '../controllers/proyectoControllador'
import md_auth from '../middlewares/authentication'
import multipart from 'connect-multiparty'

let api = express.Router()
let md_upload = multipart({uploadDir: './uploads/proyectos'})

api.get('/test',md_auth.ensureAuth,proyectoController.test)
api.post('/crear',md_auth.ensureAuth,proyectoController.saveProyecto)
api.get('/mis_proyectos',md_auth.ensureAuth,proyectoController.getMisProyectos)
api.get('/listado_general',proyectoController.getProyectos)

module.exports = api